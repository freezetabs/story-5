from django.urls import path
from . import views

urlpatterns = [
    path('', views.friends),
    path('<int:pk>/', views.delete, name = 'hapus'),
]