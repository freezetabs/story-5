from django.db import models

# Create your models here.
class Friends(models.Model):
    name = models.CharField(blank=False, max_length= 100)
    hobby = models.CharField(blank=False, max_length= 100)
    favorite_fnb = models.CharField(blank=False, max_length= 100)