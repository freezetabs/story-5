from django.shortcuts import render, redirect
from . import forms, models

# Create your views here.
def friends(request):
    if(request.method == "POST"):
        tmp = forms.formulir(request.POST)
        if(tmp.is_valid()):
            tmp2 = models.Friends()
            tmp2.name = tmp.cleaned_data["name"]
            tmp2.hobby = tmp.cleaned_data["hobby"]
            tmp2.favorite_fnb = tmp.cleaned_data["favorite_fnb"]
            tmp2.year = tmp.cleaned_data["year"]
            tmp2.save()
        return redirect("/friends")
    else:
        tmp = forms.formulir()
        tmp2 = models.Friends.objects.all()
        tmp_dictio = {
            'formulir' : tmp,
            'friends' : tmp2
        }
        return render(request, 'friends/friends.html', tmp_dictio)

def delete(request, pk):
    if(request.method == "POST"):
        tmp = forms.formulir(request.POST)
        if(tmp.is_valid()):
            tmp2 = models.Friends()
            tmp2.name = tmp.cleaned_data["name"]
            tmp2.hobby = tmp.cleaned_data["hobby"]
            tmp2.favorite_fnb = tmp.cleaned_data["favorite_fnb"]
            tmp2.year = tmp.cleaned_data["year"]
            tmp2.save()
        return redirect("/friends")
    else:
        models.Friends.objects.filter(pk = pk).delete()
        tmp = forms.formulir()
        tmp2 = models.Friends.objects.all()
        tmp_dictio = {
            'formulir' : tmp,
            'friends' : tmp2
        }
        return render(request, 'friends/friends.html', tmp_dictio)

