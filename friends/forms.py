from django import forms

class formulir(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Name',
        'type' : 'text',
        'required' : True
    }))
    hobby = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Hobby',
        'type' : 'text',
        'required' : True
    }))
    favorite_fnb = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Favorite Food/Beverages',
        'type' : 'text',
        'required' : True
    }))
    year = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Year',
        'type' : 'text',
        'required' : True
    }))
