from django.urls import path,include
from . import views

app_name = 'naufal_profile'

urlpatterns = [
    path('', views.index, name='index'),
    path('friends', include('friends.urls'), name='friends'),
]